# Desafio Luiza Labs#

Repositório referente ao desafio para vaga de desenvolvedor Android

### Linguagem Utilizada ###
 - Kotlin [Configurando Kotlin Android Studio](https://medium.com/@juanchosaravia/learn-kotlin-while-developing-an-android-app-part-1-e0f51fc1a8b3)

### Arquitetura de projeto ###
* MVP (Feature by package)

![arquitetura.PNG](https://bitbucket.org/repo/8zjprbd/images/3024662977-arquitetura.PNG)


### Dependências ###
_____

Libs

 - Dagger 2
 - RxJava 1
 - Retrofit 2
 - Kotlin 1.1.1
 - ButterKnife 8.5.1
 - Glide 3.7
 - PermissionDispatcher 2.3.1
 - OkHttp 3.4.1
 - Google Play Service(Location e maps)
________

Testes Unitários

 - Mockito Kotlin
_________

Testes de Integração

 - Espresso
 - uiautomator