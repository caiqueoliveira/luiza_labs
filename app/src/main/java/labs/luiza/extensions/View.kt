package labs.luiza.extensions

import android.view.View

/**
 * Created by Caique on 15/04/2017.
 */

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}