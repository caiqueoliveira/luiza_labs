package labs.luiza.extensions

import labs.luiza.util.TempUtil
import labs.luiza.domain.entities.City

/**
 * Created by Caique on 19/04/2017.
 */

fun City.formatDistance(): String {
    if (distance >= 1000) {
        return (distance / 1000).toString() + " Km"
    } else {
        return distance.toString() + " m"
    }
}

fun City.toFahrenheit() {
    main?.temp?.apply {
        main.temp = TempUtil.convertToFahnhereit(main.temp)
    }
    main?.tempMax?.apply {
        main.tempMax = TempUtil.convertToFahnhereit(main.tempMax)
    }
    main?.tempMin?.apply {
        main.tempMax = TempUtil.convertToFahnhereit(main.tempMax)
    }
}

fun City.toCelsius() {
    main?.temp?.apply {
        main.temp = TempUtil.convertToCelsius(main.temp)
    }
    main?.tempMax?.apply {
        main.tempMax = TempUtil.convertToCelsius(main.tempMax)
    }
    main?.tempMin?.apply {
        main.tempMax = TempUtil.convertToCelsius(main.tempMax)
    }
}