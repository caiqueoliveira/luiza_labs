package labs.luiza

/**
 * Created by Caique on 15/04/2017.
 */

class MainApplication : android.app.Application() {
    lateinit var component: MainComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerMainComponent.builder().build()
    }
}
