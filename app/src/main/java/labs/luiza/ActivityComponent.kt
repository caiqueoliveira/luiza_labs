package labs.luiza

import dagger.Subcomponent
import labs.luiza.presentation.base.PerActivity
import labs.luiza.presentation.initial.InitActivity
import labs.luiza.presentation.main.MainActivity
import labs.luiza.presentation.main.MainModule

/**
 * Created by Caique on 15/04/2017.
 */
@PerActivity
@Subcomponent(modules = arrayOf(MainModule::class))
interface ActivityComponent {
    fun inject(activity: InitActivity)
    fun inject(activity: MainActivity)
}