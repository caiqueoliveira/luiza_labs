package labs.luiza

import dagger.Component
import labs.luiza.NetworkModule
import labs.luiza.data.DataModule
import javax.inject.Singleton

/**
 * Created by Caique on 15/04/2017.
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class, DataModule::class))
interface MainComponent {
    fun uiComponent(): ActivityComponent
    fun fragmentComponent(): FragmentComponent
}
