package labs.luiza

import dagger.Subcomponent
import labs.luiza.presentation.base.PerFragment
import labs.luiza.presentation.initial.InitFragment
import labs.luiza.presentation.initial.InitModule
import labs.luiza.presentation.main.MainModule
import labs.luiza.presentation.permission.PermissionFragment

/**
 * Created by Caique on 15/04/2017.
 */
@PerFragment
@Subcomponent(modules = arrayOf(InitModule::class))
interface FragmentComponent {
    fun inject(fragment: PermissionFragment)
    fun inject(fragment: InitFragment)
}