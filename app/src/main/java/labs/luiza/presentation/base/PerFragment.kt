package labs.luiza.presentation.base

import javax.inject.Scope

/**
 * Created by Caique on 13/12/2016.
 */

@Scope
@Retention
annotation class PerFragment
