package labs.luiza.presentation.base

import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import labs.luiza.ActivityComponent
import labs.luiza.MainApplication


/**
 * Created by Caique on 15/04/2017.
 */
open class BaseActivity : AppCompatActivity() {
    private fun getMainApplication(): MainApplication {
        return application as MainApplication
    }

    fun getComponent(): ActivityComponent {
        return getMainApplication().component.uiComponent()
    }

    fun replace(@IdRes container: Int, fragment: BaseFragment) {
        supportFragmentManager.beginTransaction()
                .replace(container, fragment)
                .commit()
    }

    fun add(@IdRes container: Int, fragment: BaseFragment) {
        supportFragmentManager.beginTransaction()
                .add(container, fragment)
                .commit()
    }
}