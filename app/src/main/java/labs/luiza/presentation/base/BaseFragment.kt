package labs.luiza.presentation.base

import android.support.v4.app.Fragment
import labs.luiza.FragmentComponent
import labs.luiza.MainComponent
import labs.luiza.MainApplication



/**
 * Created by Caique on 15/04/2017.
 */
open class BaseFragment : Fragment() {
    private fun getMainApplication(): MainApplication {
        return activity.application as MainApplication
    }

    protected fun getComponent(): FragmentComponent {
        return getMainApplication().component.fragmentComponent()
    }
}