package labs.luiza.presentation.initial

import android.os.Bundle
import labs.luiza.R
import labs.luiza.presentation.base.BaseActivity

class InitActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_init)
        replace(R.id.content, InitFragment())
    }
}
