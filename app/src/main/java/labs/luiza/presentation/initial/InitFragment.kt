package labs.luiza.presentation.initial


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import labs.luiza.R
import labs.luiza.presentation.base.BaseActivity
import labs.luiza.presentation.base.BaseFragment
import labs.luiza.presentation.main.MainActivity
import labs.luiza.presentation.permission.PermissionFragment
import javax.inject.Inject

class InitFragment : BaseFragment(), InitContract.View {

    lateinit @Inject var presenter: InitContract.Presenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getComponent().inject(this)
        presenter.init(activity, this)

    }

    override fun goToMain() {
        activity.startActivity(Intent(activity, MainActivity::class.java))
        activity.finish()
    }

    override fun goToPermission() {
        (activity as BaseActivity).replace(R.id.content, PermissionFragment())
    }

}
