package labs.luiza.presentation.initial

import dagger.Module
import dagger.Provides

/**
 * Created by Caique on 15/04/2017.
 */
@Module
class InitModule {

    @Provides
    fun providerInitPresenter(): InitContract.Presenter {
        return InitPresenter()
    }
}