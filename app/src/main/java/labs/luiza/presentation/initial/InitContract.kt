package labs.luiza.presentation.initial

import android.content.Context

/**
 * Created by Caique Oliveira on 15/04/2017.
 */
interface InitContract {
    interface View{
        fun goToMain()
        fun goToPermission()
    }
    interface Presenter{
        fun init(context: Context, view: View)
    }
}