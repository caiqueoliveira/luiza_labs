package labs.luiza.presentation.initial

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat

/**
 * Created by Caique  on 15/04/2017.
 */
class InitPresenter : InitContract.Presenter {
    override fun init(context: Context, view: InitContract.View) {
        val permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            view.goToMain()
        }else{
            view.goToPermission()
        }
    }
}