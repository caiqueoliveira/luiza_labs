package labs.luiza.presentation.main

import com.google.android.gms.location.LocationRequest
import dagger.Module
import dagger.Provides
import labs.luiza.domain.repository.Repository

/**
 * Created by Caique on 15/04/2017.
 */
@Module
class MainModule {

    @Provides
    fun providesMainPresenter(repository: Repository): MainContract.Presenter {
        return MainPresenter(repository)
    }

    @Provides
    fun providesLocationRequest(): LocationRequest {
        val mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000 * 60 * 10
        mLocationRequest.fastestInterval = 1000 * 60 * 5
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        return mLocationRequest
    }
}