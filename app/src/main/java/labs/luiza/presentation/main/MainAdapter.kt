package labs.luiza.presentation.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_list_mode.view.*
import labs.luiza.R
import labs.luiza.domain.Units
import labs.luiza.domain.entities.City
import labs.luiza.extensions.formatDistance

/**
 * Created by Caique on 18/04/2017.
 */
class MainAdapter(var cities: MutableList<City>, var unit: Units) : RecyclerView.Adapter<MainViewHolder>() {
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val city = cities[position]
        with(holder.itemView) {
            when (unit) {
                Units.METRIC -> {
                    temp.text = "${city.main?.temp?.toInt()} ºC"
                }
                Units.IMPERIAL -> {
                    temp.text = "${city.main?.temp?.toInt()} ºF"
                }
                Units.DEFAULT -> {
                }
            }
            nameCity.text = "${city.name}"
            distance.text = city.formatDistance()
            //description.text = "${city.weather[0].description}"
            Glide.with(context).load(city.weather[0].getIconUrl()).into(icon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MainViewHolder(layoutInflater.inflate(R.layout.item_list_mode, parent, false))
    }

    override fun getItemCount(): Int {
        return cities.size
    }
}