package labs.luiza.presentation.main

import labs.luiza.domain.RequestCities
import labs.luiza.domain.Units
import labs.luiza.domain.entities.City
import labs.luiza.domain.repository.Repository
import labs.luiza.extensions.toCelsius
import labs.luiza.extensions.toFahrenheit
import javax.inject.Inject

/**
 * Created by Caique on 15/04/2017.
 */
class MainPresenter @Inject constructor(var repository: Repository) : MainContract.Presenter {


    var cities: MutableList<City>? = null
    lateinit var view: MainContract.View
    override fun init(view: MainContract.View) {
        this.view = view
    }

    override fun loadCityList(request: RequestCities) {
        repository.getCities(request).subscribe(
                {
                    cities = it
                    view.showList(it)
                    view.showMakers(it)
                    view.hideProgress()
                },
                { _ ->
                    view.showErrorToast()
                    view.hideProgress()
                }
        )
    }

    override fun updateMetric(unit: Units) {
        when (unit) {
            Units.METRIC -> {
                if (cities != null) {
                    cities!!.forEach(City::toCelsius)
                    view.showList(cities!!)
                } else {
                    view.showErrorWaitingListData()
                }
            }
            Units.IMPERIAL -> {
                if (cities != null) {
                    cities!!.forEach(City::toFahrenheit)
                    view.showList(cities!!)
                } else {
                    view.showErrorWaitingListData()
                }
            }
            Units.DEFAULT -> {
            }
        }
    }
}