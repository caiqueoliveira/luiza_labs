package labs.luiza.presentation.main

import android.location.Location
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_map.*
import labs.luiza.R
import labs.luiza.domain.RequestCities
import labs.luiza.domain.Units
import labs.luiza.domain.entities.City
import labs.luiza.extensions.gone
import labs.luiza.extensions.visible
import labs.luiza.presentation.base.BaseActivity
import javax.inject.Inject


class MainActivity : BaseActivity(), MainContract.View, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    lateinit @Inject var presenter: MainContract.Presenter
    lateinit @Inject var mLocationRequest: LocationRequest
    lateinit var mGoogleApiClient: GoogleApiClient
    var request: RequestCities = RequestCities()
    private var mMap: GoogleMap? = null
    var location: Location? = null
    lateinit var bottomSheetBehavior: BottomSheetBehavior<RelativeLayout>
    var mapMode = false
    var unit: Units = Units.METRIC
    var celsiusMode = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        getComponent().inject(this)
        presenter.init(this)
        initGoogleApiClient()
        initBottomSheet()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        menu.getItem(2).isVisible = mapMode
        menu.getItem(3).isVisible = !mapMode
        menu.getItem(0).isVisible = celsiusMode
        menu.getItem(1).isVisible = !celsiusMode
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_map -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                mapMode = true
            }
            R.id.action_list -> {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                mapMode = false
            }
            R.id.action_celsius -> {
                unit = Units.METRIC
                presenter.updateMetric(unit)
                celsiusMode = true
            }
            R.id.action_fahrenheit -> {
                unit = Units.IMPERIAL
                presenter.updateMetric(unit)
                celsiusMode = false
            }
        }
        invalidateOptionsMenu()
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        mGoogleApiClient.connect()
        super.onStart()
    }

    override fun onStop() {
        mGoogleApiClient.disconnect()
        super.onStop()
    }

    override fun onConnected(p0: Bundle?) {
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        location?.apply {
            request.lat = latitude
            request.lon = longitude
            presenter.loadCityList(request)
        }
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onLocationChanged(location1: Location) {
        if (location == null) {
            request.lat = location1.latitude
            request.lon = location1.longitude
            presenter.loadCityList(request)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    override fun showList(cities: MutableList<City>) {
        with(listMode) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = MainAdapter(cities, unit)
        }
    }

    fun initGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun showErrorToast() {
        Toast.makeText(this, "Erro ao carregar as informações, tente novamente mais tarde", Toast.LENGTH_LONG).show()
    }

    override fun showMakers(cities: MutableList<City>) {
        val builder = LatLngBounds.Builder()
        cities.forEach {
            val latLng = LatLng(it.coord.lat, it.coord.lon)
            val marker = MarkerOptions().position(latLng).title(it.name)
            mMap?.addMarker(marker)
            builder.include(marker.position)
        }
        val bounds = builder.build()
        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        val padding = (width * 0.15).toInt() // offset from edges of the map 10% of screen
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)
        mMap?.animateCamera(cameraUpdate)
    }

    override fun showProgress() {
        progressBar.visible()
    }

    override fun hideProgress() {
        progressBar.gone()
    }

    override fun showErrorWaitingListData() {
        Toast.makeText(this, "Aguarde o carregamento das cidades", Toast.LENGTH_LONG).show()
    }

    fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        mapMode = false
                        invalidateOptionsMenu()
                        bottomSheet.setBackgroundColor(ContextCompat.getColor(this@MainActivity, android.R.color.white))
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        mapMode = true
                        invalidateOptionsMenu()
                        bottomSheet.setBackgroundColor(ContextCompat.getColor(this@MainActivity, android.R.color.transparent))
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        bottomSheet.setBackgroundColor(ContextCompat.getColor(this@MainActivity, android.R.color.transparent))
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
    }
}
