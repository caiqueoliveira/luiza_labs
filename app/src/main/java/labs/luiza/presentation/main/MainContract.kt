package labs.luiza.presentation.main

import labs.luiza.domain.RequestCities
import labs.luiza.domain.Units
import labs.luiza.domain.entities.City

/**
 * Created by Caique on 15/04/2017.
 */
interface MainContract {
    interface View {
        fun showList(cities: MutableList<City>)
        fun showMakers(cities: MutableList<City>)
        fun showErrorToast()
        fun showProgress()
        fun hideProgress()
        fun showErrorWaitingListData()
    }

    interface Presenter {
        fun init(view: View)
        fun loadCityList(request: RequestCities)
        fun updateMetric(unit: Units)
    }
}