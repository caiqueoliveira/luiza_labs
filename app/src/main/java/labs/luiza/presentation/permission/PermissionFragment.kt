package labs.luiza.presentation.permission


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.fragment_permission.*
import labs.luiza.R
import labs.luiza.extensions.gone
import labs.luiza.extensions.visible
import labs.luiza.presentation.base.BaseFragment
import labs.luiza.presentation.main.MainActivity
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.RuntimePermissions

/**
 * Runtime Permissions with anotations using @see <a href="https://github.com/hotchemi/PermissionsDispatcher">Permissiion Dispatcher</a>
 */
@RuntimePermissions
class PermissionFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_permission, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    @OnClick(R.id.btnExit, R.id.btnNext, R.id.btnConfig)
    fun onClick(view: View) {
        when (view.id) {
            R.id.btnExit -> {
                activity.finish()
            }
            R.id.btnNext -> {
                PermissionFragmentPermissionsDispatcher.locationGrantedWithCheck(this)
            }
            R.id.btnConfig -> {
                manualConfiguration()
            }
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationGranted()
        }
    }

    fun manualConfiguration() {
        Toast.makeText(activity, "Permissão negada, habilite-a nas configurações do aplicativo", Toast.LENGTH_SHORT).show()
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun locationGranted() {
        activity.startActivity(Intent(activity, MainActivity::class.java))
        activity.finish()
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    fun neverAsk() {
        btnNext.gone()
        btnConfig.visible()
    }

}
