package labs.luiza.util

/**
 * Created by Caique on 19/04/2017.
 */
object DistanceUtil {
    fun calculateDistance(latA: Double, lngA: Double, latB: Double, lngB: Double): Int {
        val theta = lngA - lngB
        var dist = Math.sin(deg2rad(latA)) * Math.sin(deg2rad(latB)) + Math.cos(deg2rad(latA)) * Math.cos(deg2rad(latB)) * Math.cos(deg2rad(theta))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist *= 60.0 * 1.1515
        dist *= 1.609344 //distance in kilometers

        return dist.toInt()
    }

    fun isValid(distance: Int): Boolean {
        return 50000 > distance
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180 / Math.PI
    }

}