package labs.luiza.util

/**
 * Created by Caique on 19/04/2017.
 */

object TempUtil {
    fun convertToFahnhereit(temp: Double): Double {
        return 9 * (temp / 5) + 32
    }

    fun convertToCelsius(temp: Double): Double {
        return (temp - 32) * 5 / 9
    }
}