package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Clouds(

        @field:SerializedName("all")
        @field:Expose
        val all: Int? = null
)