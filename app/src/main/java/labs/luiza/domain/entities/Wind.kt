package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Wind(

        @field:SerializedName("deg")
        @field:Expose
        val deg: Double? = null,

        @field:SerializedName("speed")
        @field:Expose
        val speed: Double? = null
)