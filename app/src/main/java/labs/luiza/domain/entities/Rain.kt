package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Rain(

        @field:SerializedName("3h")
        @field:Expose
        val jsonMember3h: Double? = null
)