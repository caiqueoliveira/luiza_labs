package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class City(

        @field:SerializedName("rain")
        @field:Expose
        val rain: Rain? = null,

        @field:SerializedName("coord")
        @field:Expose
        val coord: Coord,

        @field:SerializedName("name")
        @field:Expose
        val name: String? = null,

        @field:SerializedName("weather")
        @field:Expose
        val weather: List<WeatherItem>,

        @field:SerializedName("main")
        @field:Expose
        val main: Main? = null,

        @field:SerializedName("id")
        @field:Expose
        val id: Int? = null,

        @field:SerializedName("clouds")
        @field:Expose
        val clouds: Clouds? = null,

        @field:SerializedName("wind")
        @field:Expose
        val wind: Wind? = null,

        var distance: Int)
{

}