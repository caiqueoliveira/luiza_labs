package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class WeatherItem {

    @field:SerializedName("description")
    @field:Expose
    val description: String? = null

    @field:SerializedName("main")
    @field:Expose
    val main: String? = null

    @field:SerializedName("id")
    @field:Expose
    val id: Int? = null

    val icon: String?=null

    fun getIconUrl(): String {
        return "http://openweathermap.org/img/w/$icon.png"
    }
}