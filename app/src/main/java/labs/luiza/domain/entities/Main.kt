package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Main(

        @field:SerializedName("temp")
        @field:Expose
        var temp: Double,

        @field:SerializedName("temp_min")
        @field:Expose
        val tempMin: Double? = null,

        @field:SerializedName("humidity")
        @field:Expose
        val humidity: Int? = null,

        @field:SerializedName("temp_max")
        @field:Expose
        var tempMax: Double
)