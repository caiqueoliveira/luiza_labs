package labs.luiza.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Coord(

        @field:SerializedName("lon")
        @field:Expose
        val lon: Double,

        @field:SerializedName("lat")
        @field:Expose
        val lat: Double
)