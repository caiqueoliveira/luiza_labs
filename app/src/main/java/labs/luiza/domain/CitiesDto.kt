package labs.luiza.domain

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import labs.luiza.domain.entities.City

data class CitiesDto(

        @field:SerializedName("calctime")
        @field:Expose
        val calctime: Double? = null,

        @field:SerializedName("cnt")
        @field:Expose
        val cnt: Int? = null,

        @field:SerializedName("cod")
        @field:Expose
        val cod: String? = null,

        @field:SerializedName("list")
        @field:Expose
        val list: MutableList<City>
)