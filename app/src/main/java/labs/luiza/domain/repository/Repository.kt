package labs.luiza.domain.repository

import labs.luiza.domain.RequestCities
import labs.luiza.domain.entities.City
import rx.Observable

/**
 * Created by Caique on 16/04/2017.
 */
interface Repository {
    fun getCities(request: RequestCities): Observable<MutableList<City>>
}