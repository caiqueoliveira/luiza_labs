package labs.luiza.domain

/**
 * Created by Caique on 19/04/2017.
 */
enum class Units {
    METRIC,
    IMPERIAL,
    DEFAULT
}