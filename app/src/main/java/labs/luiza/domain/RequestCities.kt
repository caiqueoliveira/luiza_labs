package labs.luiza.domain

/**
 * Created by Caique on 16/04/2017.
 */
data class RequestCities(var lat: Double?=null, var lon: Double?=null, val cnt: Int = 10, val units: Units = Units.METRIC)