package labs.luiza.data

import labs.luiza.BuildConfig
import labs.luiza.util.DistanceUtil
import labs.luiza.data.client.Service
import labs.luiza.domain.RequestCities
import labs.luiza.domain.entities.City
import labs.luiza.domain.repository.Repository
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Caique on 16/04/2017.
 */
class Manager @Inject constructor(var service: Service) : Repository {
    override fun getCities(request: RequestCities): Observable<MutableList<City>> {
        return service.requestCitiesNext(request.lat!!, request.lon!!, request.cnt, request.units, BuildConfig.API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.list }
                .flatMapIterable { it -> it }
                .map { it.distance = DistanceUtil.calculateDistance(it.coord.lat, it.coord.lon, request.lat!!, request.lon!!); it }
                .filter { DistanceUtil.isValid(it.distance) }
                .toList()
    }
}