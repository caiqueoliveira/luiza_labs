package labs.luiza.data

import dagger.Module
import dagger.Provides
import labs.luiza.data.client.Service
import labs.luiza.domain.repository.Repository
import retrofit2.Retrofit

/**
 * Created by Caique on 16/04/2017.
 */
@Module
class DataModule {

    @Provides
    fun providesService(retrofit: Retrofit): Service {
        return retrofit.create(Service::class.java)
    }

    @Provides
    fun providesManager(service: Service): Repository {
        return Manager(service)
    }
}