package labs.luiza.data.client

import labs.luiza.domain.CitiesDto
import labs.luiza.domain.Units
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

/**
 * Created by Caique on 16/04/2017.
 */
interface Service {
    @POST("/data/2.5/find")
    fun requestCitiesNext(@Query("lat") lat: Double,
                          @Query("lon") long: Double,
                          @Query("cnt") cnt: Int,
                          @Query("units") units:Units,
                          @Query("appid") appid: String): Observable<CitiesDto>
}