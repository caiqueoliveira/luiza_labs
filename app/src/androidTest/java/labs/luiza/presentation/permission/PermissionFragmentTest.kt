package labs.luiza.presentation.permission

import android.os.Build
import android.support.test.InstrumentationRegistry
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.InstrumentationRegistry.getTargetContext
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.UiDevice
import labs.luiza.R
import labs.luiza.presentation.initial.InitActivity
import labs.luiza.presentation.main.MainActivity
import labs.util.UiAutomatorUtils.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by Caique on 20/04/2017.
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class PermissionFragmentTest {

    @Rule @JvmField
    var activityRule = ActivityTestRule<InitActivity>(InitActivity::class.java,false,false)
    lateinit var mDevice: UiDevice

    @Before
    fun setUp() {
        resetPermissions()
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        Intents.init()
    }

    @Test
    fun shouldBeClicknextButtonAndDenyPermission() {
        activityRule.launchActivity(null)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            resetPermissions()
            onView(withId(R.id.btnNext)).check(matches(isDisplayed()))
            onView(withId(R.id.btnNext)).perform(click())
            denyCurrentPermission(mDevice)
            onView(withId(R.id.btnNext)).check(matches(isDisplayed()))
            resetPermissions()
        }
    }

    @Test
    fun shouldBeClickNextButtonAndAllowPermission() {
        activityRule.launchActivity(null)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            resetPermissions()
            onView(withId(R.id.btnNext)).check(matches(isDisplayed()))
            onView(withId(R.id.btnNext)).perform(click())
            allowCurrentPermission(mDevice)
            intended(hasComponent(MainActivity::class.java.name))
            resetPermissions()
        }
    }

    @Test
    fun shouldBeClickInNeverAskAgainAndShowButtonConfiguration() {
        activityRule.launchActivity(null)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            resetPermissions()
            onView(withId(R.id.btnNext)).perform(click())
            denyCurrentPermission(mDevice)
            onView(withId(R.id.btnNext)).perform(click())
            denyCurrentPermissionPermanently(mDevice)
            onView(withId(R.id.btnConfig)).check(matches(isDisplayed()))
            resetPermissions()
        }
    }

    @Test
    fun shouldBeExitApp() {
        activityRule.launchActivity(null)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            onView(withId(R.id.btnExit)).perform(click())
        }
    }

    @After
    fun tearDown() {
        Intents.release()
    }

    fun resetPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getInstrumentation().uiAutomation.executeShellCommand(
                    "pm reset-permissions " + getTargetContext().packageName
                            + " android.permission.ACCESS_FINE_LOCATION")

        }
    }
}