package labs.luiza.presentation.main

import android.os.Build
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.uiautomator.UiDevice
import kotlinx.android.synthetic.main.content_map.*
import labs.luiza.R
import labs.luiza.extensions.gone
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Caique on 21/04/2017.
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule @JvmField
    var activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, false, false)
    lateinit var mDevice: UiDevice
    @Before
    fun setUp() {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        grantPermission()
    }

    @Test
    fun shouldBeShowRecyclerViewWithoutMockRequest() {
       activityRule.launchActivity(null)
        onView(withId(R.id.listMode)).check(matches(isDisplayed()))
    }

    @Test
    fun shouldBeClickFahrenheitButton() {
       activityRule.launchActivity(null)
        onView(withId(R.id.action_fahrenheit)).perform(click())
    }

    fun grantPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand(
                    "pm grant " + InstrumentationRegistry.getTargetContext().packageName
                            + " android.permission.ACCESS_FINE_LOCATION")

        }
    }
}