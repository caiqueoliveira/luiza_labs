package labs.luiza.presentation.main

import com.nhaarman.mockito_kotlin.*
import labs.luiza.domain.RequestCities
import labs.luiza.domain.entities.City
import labs.luiza.domain.repository.Repository
import org.junit.Before
import org.junit.Test
import rx.Observable

/**
 * Created by Caique on 20/04/2017.
 */

class MainPresenterTest {
    lateinit var presenter: MainContract.Presenter
    var request: RequestCities = mock()
    var repository: Repository = mock()
    var view: MainContract.View = mock()
    var cities: MutableList<City> = mock()
    @Before
    fun setUp() {
        presenter = MainPresenter(repository)
        presenter.init(view)
    }

    @Test
    fun loadCitiesTest() {
        whenever(repository.getCities(request)).thenReturn(Observable.just(cities))

        presenter.loadCityList(request)

        verify(repository).getCities(request)
        verifyNoMoreInteractions(repository)
        verify(view).showList(cities)
        verify(view).showMakers(cities)
        verify(view).hideProgress()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun loadCitiesTestWithException() {
        whenever(repository.getCities(request)).thenReturn(Observable.error(Throwable()))

        presenter.loadCityList(request)

        verify(repository).getCities(request)
        verifyNoMoreInteractions(repository)
        verify(view).showErrorToast()
        verify(view).hideProgress()
        verifyNoMoreInteractions(view)
    }
}
