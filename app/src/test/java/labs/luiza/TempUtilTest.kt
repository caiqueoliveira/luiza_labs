package labs.luiza

import labs.luiza.util.TempUtil
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Created by Caique on 20/04/2017.
 */
class TempUtilTest {
    @Test
    fun convertToFahnhereitTest() {
        assertTrue(TempUtil.convertToFahnhereit(0.0).toInt() == 32)
    }

    @Test
    fun convertToCelsiusTest() {
        assertTrue(TempUtil.convertToCelsius(32.0).toInt() == 0)
    }
}