package labs.luiza

import labs.luiza.util.DistanceUtil
import org.junit.Test
import kotlin.test.assertTrue

/**
 * Created by Caique on 20/04/2017.
 */
class DistanceUtilTest {

    @Test
    fun calculateDistance() {
        assertTrue(DistanceUtil.calculateDistance(55.0, 55.0, 55.0, 55.0) == 0)
    }

    @Test
    fun isValidTest() {
        kotlin.test.assertTrue(DistanceUtil.isValid(1000))
    }
}