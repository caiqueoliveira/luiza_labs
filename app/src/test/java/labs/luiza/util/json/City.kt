package labs.luiza.util.json

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import labs.luiza.domain.entities.City


/**
 * Created by Caique on 20/04/2017.
 */
object City {
    fun getCitie(): MutableList<City> {
        val jsonArray: String = "{\n" +
                "  \"message\": \"accurate\",\n" +
                "  \"cod\": \"200\",\n" +
                "  \"count\": 10,\n" +
                "  \"list\": [\n" +
                "    {\n" +
                "      \"id\": 495260,\n" +
                "      \"name\": \"Shcherbinka\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.4997,\n" +
                "        \"lon\": 37.5597\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 564517,\n" +
                "      \"name\": \"Dubrovitsy\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.4397,\n" +
                "        \"lon\": 37.4867\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 59,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 570578,\n" +
                "      \"name\": \"Butovo\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.5483,\n" +
                "        \"lon\": 37.5797\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 545782,\n" +
                "      \"name\": \"Kommunarka\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.5695,\n" +
                "        \"lon\": 37.4893\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.1,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 71,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 6417490,\n" +
                "      \"name\": \"Lesparkkhoz\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.5431,\n" +
                "        \"lon\": 37.6014\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 526736,\n" +
                "      \"name\": \"Sed’moy Mikrorayon\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.5622,\n" +
                "        \"lon\": 37.5797\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 473051,\n" +
                "      \"name\": \"Vlas’yevo\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.4603,\n" +
                "        \"lon\": 37.3794\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 59,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 578680,\n" +
                "      \"name\": \"Bachurino\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.58,\n" +
                "        \"lon\": 37.52\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.1,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 554629,\n" +
                "      \"name\": \"Shestoy Mikrorayon\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.5667,\n" +
                "        \"lon\": 37.5833\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 70,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 508101,\n" +
                "      \"name\": \"Podolsk\",\n" +
                "      \"coord\": {\n" +
                "        \"lat\": 55.4242,\n" +
                "        \"lon\": 37.5547\n" +
                "      },\n" +
                "      \"main\": {\n" +
                "        \"temp\": 31.08,\n" +
                "        \"pressure\": 1024,\n" +
                "        \"humidity\": 60,\n" +
                "        \"temp_min\": 30.2,\n" +
                "        \"temp_max\": 32\n" +
                "      },\n" +
                "      \"dt\": 1492660800,\n" +
                "      \"wind\": {\n" +
                "        \"speed\": 8.95,\n" +
                "        \"deg\": 350\n" +
                "      },\n" +
                "      \"sys\": {\n" +
                "        \"country\": \"\"\n" +
                "      },\n" +
                "      \"rain\": null,\n" +
                "      \"snow\": null,\n" +
                "      \"clouds\": {\n" +
                "        \"all\": 0\n" +
                "      },\n" +
                "      \"weather\": [\n" +
                "        {\n" +
                "          \"id\": 800,\n" +
                "          \"main\": \"Clear\",\n" +
                "          \"description\": \"Sky is Clear\",\n" +
                "          \"icon\": \"01d\"\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}"

        val listType = object : TypeToken<MutableList<City>>() {}.type

        val cities: MutableList<City> = Gson().fromJson(jsonArray, listType)
        return cities
    }
}