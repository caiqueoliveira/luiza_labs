package labs.luiza.extensions

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import labs.luiza.domain.entities.City
import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by Caique on 20/04/2017.
 */
class CityKtTest {

    var city: City = mock()
    @Test
    fun formatDistanceTest() {
        whenever(city.distance).thenReturn(1000)
        assertEquals("1 Km",city.formatDistance())
    }


}